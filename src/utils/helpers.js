import MD5 from "crypto-js/md5"

export const hashPassword = (password) => {
	return MD5(password)
}

export const validateEmail = (email) => {
	const validEmailRegex = RegExp(
		//eslint-disable-next-line
		/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
	)
	return validEmailRegex.test(String(email).toLowerCase())
}

export const escapeRegExp = (string) => {
	return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&") // $& means the whole matched string
}

export const getFilteredContent = (content, query, key = "title") => {
	const re = new RegExp(escapeRegExp(query), "i")
	const isMatch = (result) => re.test(result[key])
	return content.filter(isMatch)
}

export const getLastShowed = (lastShowedList, newId, maxItems = 5) => {
	// check if the current list has a newId
	if (lastShowedList.includes(newId)) {
		const lastShowedListWithoutNewId = lastShowedList.filter(
			(contentId) => contentId !== newId
		)
		return [...lastShowedListWithoutNewId, newId].slice(-maxItems)
	} else {
		return [...lastShowedList, newId].slice(-maxItems)
	}
}
