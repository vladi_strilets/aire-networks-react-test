import { createSelectorCreator, defaultMemoize } from "reselect"
import isEqual from "react-fast-compare"

export const createDeepEqualSelector = createSelectorCreator(
	defaultMemoize,
	isEqual
)
