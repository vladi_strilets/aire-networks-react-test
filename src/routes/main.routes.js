import React from "react"
import { Switch } from "react-router-dom"
import PrivateRoute from "../components/auth-routes/PrivateRoute"
import PublicRoute from "../components/auth-routes/PublicRoute"
import HomePage from "../pages/Home.page"
import LoginPage from "../pages/Login.page"
import NoContentPage from "../pages/NoContent.page"
import PlayerPage from "../pages/Player.page"
import ROUTES from "./routes.types"

const PublicRoutes = [
	{ path: ROUTES.LOGIN, component: LoginPage, restricted: true },
]

const PrivateRoutes = [
	{ path: ROUTES.HOME, component: HomePage },
	{ path: ROUTES.PLAYER, component: PlayerPage },
]

const MainRoutes = () => {
	return (
		<Switch>
			{PublicRoutes.map((routeProps, index) => (
				<PublicRoute key={index} {...routeProps} exact />
			))}
			{PrivateRoutes.map((routeProps, index) => (
				<PrivateRoute key={index} {...routeProps} exact />
			))}
			<NoContentPage />
		</Switch>
	)
}

export default MainRoutes
