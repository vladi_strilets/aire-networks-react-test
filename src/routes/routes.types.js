const ROUTES = {
	HOME: "/",
	LOGIN: "/login",
	PLAYER: "/player/:id",
}

export default ROUTES
