import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useParams } from "react-router-dom"
import { Container, Loader, Message } from "semantic-ui-react"
import Player from "../components/player/Player"
import PlayerDescription from "../components/player/PlayerDescription"
import {
	fetchPlayerAsync,
	removePlayerData,
} from "../redux/player/player.actions"
import {
	selectIsPlayerLoaded,
	selectPlayerError,
} from "../redux/player/player.selectors"

const PlayerPage = () => {
	const { id } = useParams()
	const dispatch = useDispatch()

	const isPlayerLoaded = useSelector(selectIsPlayerLoaded)
	const playerError = useSelector(selectPlayerError)

	useEffect(() => {
		dispatch(fetchPlayerAsync(id))

		return () => {
			dispatch(removePlayerData())
		}
	}, [dispatch, id])

	if (!isPlayerLoaded) {
		return <Loader active />
	}

	if (playerError) {
		return <Message error content={playerError} />
	}

	return (
		<Container>
			<Player />
			<PlayerDescription />
		</Container>
	)
}

export default PlayerPage
