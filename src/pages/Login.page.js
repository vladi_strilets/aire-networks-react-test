import React from "react"
import { Grid, Header, Segment } from "semantic-ui-react"
import styled from "styled-components"
import LoginForm from "../components/forms/LoginForm"

const BackgroundContainer = styled.div`
	height: 100%;
	width: 100%;
	background-color: #f7f8fa;
`

const CustomGrid = styled(Grid)`
	height: 100%;
	margin-top: 0;
	margin-bottom: 0;
`

const CustomGridColumn = styled(Grid.Column)`
	max-width: 600px;
`

const LoginPage = () => {
	return (
		<BackgroundContainer>
			<CustomGrid verticalAlign="middle" centered container>
				<CustomGridColumn>
					<Segment>
						<Header as="h2" content="Login" textAlign="center" />
						<LoginForm />
					</Segment>
				</CustomGridColumn>
			</CustomGrid>
		</BackgroundContainer>
	)
}

export default LoginPage
