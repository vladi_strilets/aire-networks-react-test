import React from "react"
import { Link } from "react-router-dom"
import { Button, Container, Header } from "semantic-ui-react"
import ROUTES from "../routes/routes.types"

const NoContentPage = () => {
	return (
		<Container>
			<Header as="h1" content="Página no existe" />
			<Button as={Link} to={ROUTES.HOME} basic content="Ir a la home" />
		</Container>
	)
}

export default NoContentPage
