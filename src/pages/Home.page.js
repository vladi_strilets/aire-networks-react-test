import React from "react"
import { Container } from "semantic-ui-react"
import ContentComponent from "../components/contents/ContentComponent"
import UserFavContent from "../components/contents/UserFavContent"
import UserLastShowedContent from "../components/contents/UserLastShowedContent"

const HomePage = () => {
	return (
		<Container>
			<UserFavContent />
			<UserLastShowedContent />
			<ContentComponent />
		</Container>
	)
}

export default HomePage
