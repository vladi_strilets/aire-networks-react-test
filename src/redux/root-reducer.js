import { combineReducers } from "redux"
import { persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"
import authReducer from "./auth/auth.reducer"
import dataReducer from "./data/data.reducer"
import playerReducer from "./player/player.reducer"

const persistConfig = {
	key: "root",
	storage,
	whitelist: ["auth"],
}

const rootReducer = persistReducer(
	persistConfig,
	combineReducers({
		auth: authReducer,
		data: dataReducer,
		player: playerReducer,
	})
)

export default rootReducer
