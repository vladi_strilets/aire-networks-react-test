import { applyMiddleware, compose, createStore } from "redux"
import { createLogger } from "redux-logger"
import { persistStore } from "redux-persist"
import thunk from "redux-thunk"
import rootReducer from "./root-reducer"

const middlewares = [thunk]

const logger = createLogger({
	predicate: true,
})

if (process.env.NODE_ENV === "development") {
	middlewares.push(logger)
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
	rootReducer,
	composeEnhancers(applyMiddleware(...middlewares))
)

export const persistor = persistStore(store)

export default { store, persistor }
