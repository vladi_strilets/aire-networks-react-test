import AUTH_ACTIONS from "./auth.types"

const INITIAL_STATE = {
	user: {
		email: null,
		name: null,
		avatar: null,
	},
	token: null,
	isAuth: false,
}

const authReducer = (state = INITIAL_STATE, action) => {
	const { type, payload } = action
	switch (type) {
		case AUTH_ACTIONS.SET_USER:
			return {
				...state,
				user: { ...state.user, email: payload.email },
				token: payload.token,
				isAuth: true,
			}

		case AUTH_ACTIONS.SET_USER_PROFILE:
			return {
				...state,
				user: { ...state.user, ...payload },
			}

		// LOGOUT ACTIONS
		case AUTH_ACTIONS.LOGOUT:
			return INITIAL_STATE

		// TOKEN ACTIONS
		default:
			return state
	}
}

export default authReducer
