import AUTH_ACTIONS from "./auth.types"

export const setUser = (payload) => ({
	type: AUTH_ACTIONS.SET_USER,
	payload,
})

export const setUserProfile = (payload) => ({
	type: AUTH_ACTIONS.SET_USER_PROFILE,
	payload,
})

export const logout = () => ({
	type: AUTH_ACTIONS.LOGOUT,
})
