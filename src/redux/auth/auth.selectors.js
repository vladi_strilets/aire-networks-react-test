import { createSelector } from "reselect"

const authSelector = (state) => state.auth

export const selectUserEmail = createSelector(
	[authSelector],
	(authReducer) => authReducer.user.email
)

export const selectIsAuth = createSelector(
	[authSelector],
	(authReducer) => authReducer.isAuth
)

export const selectToken = createSelector(
	[authSelector],
	(authReducer) => authReducer.user.token
)

export const selectUserAvatar = createSelector(
	[authSelector],
	(authReducer) => authReducer.user.avatar
)
export const selectUserName = createSelector(
	[authSelector],
	(authReducer) => authReducer.user.name
)
