import Axios from "axios"
import { API_URL } from "../../utils/const"
import PLAYER_ACTIONS from "./player.types"

export const fetchPlayerStart = () => ({
	type: PLAYER_ACTIONS.FETCH_PLAYER_START,
})

export const fetchPlayerSucceess = (data) => ({
	type: PLAYER_ACTIONS.FETCH_PLAYER_SUCCESS,
	payload: data,
})

export const fetchPlayerFailure = (error) => ({
	type: PLAYER_ACTIONS.FETCH_PLAYER_FAILURE,
	payloas: error,
})

export const fetchPlayerAsync = (playerId) => {
	return async (dispatch, getState) => {
		dispatch(fetchPlayerStart())

		const token = getState().auth.user.token

		const formData = new FormData()
		formData.append("token", token)
		formData.append("device", "Web")
		formData.append("id", playerId)

		try {
			const { data } = await Axios({
				url: `${API_URL}/Play.php`,
				method: "POST",
				data: formData,
			})

			if ("error" in data) {
				dispatch(fetchPlayerFailure(data.message))
				return
			}

			dispatch(fetchPlayerSucceess(data))
		} catch (err) {
			console.error(err.message)
			dispatch(fetchPlayerFailure(err.message))
		}
	}
}

export const removePlayerData = () => ({
	type: PLAYER_ACTIONS.REMOVE_PLAYER_DATA,
})
