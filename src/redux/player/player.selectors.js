import { createSelector } from "reselect"
import { createDeepEqualSelector } from "../../utils/selectors"

const playerSelector = (state) => state.player

export const selectIsPlayerFetching = createSelector(
	[playerSelector],
	(playerReducer) => playerReducer.isFetching
)

export const selectIsPlayerLoaded = createSelector(
	[playerSelector],
	(playerReducer) => playerReducer.isLoaded
)

export const selectPlayerError = createSelector(
	[playerSelector],
	(playerReducer) => playerReducer.error
)

export const selectPlayerUrl = createSelector(
	[playerSelector],
	(playerReducer) => playerReducer.player.url
)

export const selectPlayerData = createDeepEqualSelector(
	[playerSelector],
	(playerReducer) => playerReducer.player
)
