import PLAYER_ACTIONS from "./player.types"

const INITIAL_STATE = {
	player: {
		id: null,
		title: null,
		cover: null,
		url: null,
		duration: null,
		rating: null,
		votes: null,
		totalVotes: null,
	},
	isFetching: false,
	isLoaded: false,
	error: null,
}

const playerReducer = (state = INITIAL_STATE, action) => {
	const { type, payload } = action

	switch (type) {
		case PLAYER_ACTIONS.FETCH_PLAYER_START:
			return {
				...state,
				isFetching: true,
				error: false,
			}
		case PLAYER_ACTIONS.FETCH_PLAYER_FAILURE:
			return {
				...state,
				isFetching: false,
				isLoaded: true,
				error: payload,
			}
		case PLAYER_ACTIONS.FETCH_PLAYER_SUCCESS:
			return {
				...state,
				isFetching: false,
				isLoaded: true,
				player: payload,
			}

		case PLAYER_ACTIONS.REMOVE_PLAYER_DATA:
			return INITIAL_STATE

		default:
			return state
	}
}
export default playerReducer
