import Axios from "axios"
import { API_URL } from "../../utils/const"
import DATA_ACTIONS from "./data.types"

import { setUserProfile } from "../auth/auth.actions"
import { batch } from "react-redux"

export const fetchDataStart = () => ({
	type: DATA_ACTIONS.FETCH_DATA_START,
})

export const fetchDataSucceess = (data) => ({
	type: DATA_ACTIONS.FETCH_DATA_SUCCESS,
	payload: data,
})

export const fetchDataFailure = (error) => ({
	type: DATA_ACTIONS.FETCH_DATA_FAILURE,
	payloas: error,
})

export const fetchDataAsync = () => {
	return async (dispatch, getState) => {
		dispatch(fetchDataStart())

		const token = getState().auth.user.token

		const formData = new FormData()
		formData.append("token", token)
		formData.append("device", "Web")

		try {
			const { data } = await Axios({
				url: `${API_URL}/GetView.php`,
				method: "POST",
				data: formData,
			})

			if ("error" in data) {
				dispatch(fetchDataFailure(data.message))
				return
			}

			const userData = {
				favs: data.user.favs,
				lastShowed: data.user.lastShowed,
			}

			const payload = {
				userData,
				contents: data.contents,
			}

			const userProfile = {
				name: data.user.name,
				avatar: data.user.avatar,
			}

			batch(() => {
				dispatch(fetchDataSucceess(payload))
				dispatch(setUserProfile(userProfile))
			})
		} catch (err) {
			console.error(err.message)
			dispatch(fetchDataFailure(err.message))
		}
	}
}

export const removeFromFavs = (id) => ({
	type: DATA_ACTIONS.REMOVE_FROM_FAVS,
	payload: id,
})

export const addToFavs = (id) => ({
	type: DATA_ACTIONS.ADD_TO_FAVS,
	payload: id,
})

export const removeData = () => ({
	type: DATA_ACTIONS.REMOVE_DATA,
})

export const addToLastShowed = (id) => ({
	type: DATA_ACTIONS.ADD_TO_LAST_SHOWED,
	payload: id,
})
