import { getLastShowed } from "../../utils/helpers"
import DATA_ACTIONS from "./data.types"

const INITIAL_STATE = {
	userData: {
		favs: [],
		lastShowed: [],
	},
	contents: [],
	isFetching: false,
	isLoaded: false,
	error: null,
}

const dataReducer = (state = INITIAL_STATE, action) => {
	const { type, payload } = action

	switch (type) {
		case DATA_ACTIONS.FETCH_DATA_START:
			return { ...state, isFetching: true, error: null }
		case DATA_ACTIONS.FETCH_DATA_FAILURE:
			return { ...state, isFetching: false, isLoaded: false, error: payload }
		case DATA_ACTIONS.FETCH_DATA_SUCCESS:
			return {
				...state,
				isFetching: false,
				isLoaded: true,
				userData: { ...state.userData, ...payload.userData },
				contents: payload.contents,
			}
		case DATA_ACTIONS.REMOVE_FROM_FAVS:
			return {
				...state,
				userData: {
					...state.userData,
					favs: state.userData.favs.filter((id) => id !== payload),
				},
			}
		case DATA_ACTIONS.ADD_TO_FAVS:
			return {
				...state,
				userData: {
					...state.userData,
					favs: [...state.userData.favs, payload],
				},
			}

		case DATA_ACTIONS.REMOVE_DATA:
			return INITIAL_STATE

		case DATA_ACTIONS.ADD_TO_LAST_SHOWED:
			return {
				...state,
				userData: {
					...state.userData,
					lastShowed: getLastShowed(state.userData.lastShowed, payload),
				},
			}
		default:
			return state
	}
}

export default dataReducer
