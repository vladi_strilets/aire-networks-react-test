import { createSelector } from "reselect"
import { createDeepEqualSelector } from "../../utils/selectors"

const dataSelector = (state) => state.data

export const selectIsDataFetching = createSelector(
	[dataSelector],
	(dataReducer) => dataReducer.isFetching
)

export const selectIsDataLoaded = createSelector(
	[dataSelector],
	(dataReducer) => dataReducer.isLoaded
)

export const selectDataError = createSelector(
	[dataSelector],
	(dataReducer) => dataReducer.error
)

export const selectContents = createDeepEqualSelector(
	[dataSelector],
	(dataReducer) => dataReducer.contents
)

export const selectUsersFavs = createDeepEqualSelector(
	(state) => state.data.contents,
	(state) => state.data.userData.favs,
	(contents, favs) =>
		contents
			.filter((content) => favs.includes(content.id))
			.sort((idA, idB) => favs.indexOf(idA.id) - favs.indexOf(idB.id))
)

export const selectUserFavsIds = createDeepEqualSelector(
	[dataSelector],
	(dataReducer) => dataReducer.userData.favs
)

export const selectUserLastShowed = createDeepEqualSelector(
	(state) => state.data.contents,
	(state) => state.data.userData.lastShowed,
	(contents, lastShowed) =>
		contents
			.filter((content) => lastShowed.includes(content.id))
			.sort(
				(idA, idB) => lastShowed.indexOf(idA.id) - lastShowed.indexOf(idB.id)
			)
)

export const makeIsFavSelector = () =>
	createSelector(
		(state) => state.data.userData.favs,
		(_, contentId) => contentId,
		(favs, contentId) => favs.includes(contentId)
	)
