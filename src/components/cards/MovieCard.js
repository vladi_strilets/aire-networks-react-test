import React, { useMemo } from "react"
import PropTypes from "prop-types"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Button, Card } from "semantic-ui-react"
import styled from "styled-components"
import { addToFavs, removeFromFavs } from "../../redux/data/data.actions"
import { makeIsFavSelector } from "../../redux/data/data.selector"
import ROUTES from "../../routes/routes.types"

const ImageContainer = styled.div`
	height: 280px;
	overflow: hidden;
	background-size: cover;
	background-position: center;
	background-image: url("path/to/image.jpg");
	background-image: ${(props) => props.src && "url(" + props.src + ")"};
`

const MovieCard = ({ id, title, cover, section }) => {
	const history = useHistory()
	const dispatch = useDispatch()

	// memoized selector
	const selectIsFav = useMemo(makeIsFavSelector, [])
	const isFav = useSelector((state) => selectIsFav(state, id))

	// action to add or remove from favs
	const onButtonClick = (e) => {
		e.stopPropagation()

		if (isFav) {
			dispatch(removeFromFavs(id))
		} else {
			dispatch(addToFavs(id))
		}
	}

	// go to player
	const onClick = () => {
		history.push(ROUTES.PLAYER.replace(":id", id))
	}

	return (
		<Card onClick={onClick}>
			<ImageContainer src={cover} />
			<Card.Content>
				<Card.Description content={title} />
				<Card.Meta>
					<span>{section}</span>
				</Card.Meta>
			</Card.Content>
			{id && (
				<Card.Content extra>
					<Button
						fluid
						basic
						content={isFav ? "Eliminar de favoritos" : "Añadir a favoritos"}
						onClick={onButtonClick}
						color={isFav ? "red" : "blue"}
					/>
				</Card.Content>
			)}
		</Card>
	)
}

MovieCard.propTypes = {
	id: PropTypes.string,
	title: PropTypes.string,
	cover: PropTypes.string,
	section: PropTypes.string,
}

export default MovieCard
