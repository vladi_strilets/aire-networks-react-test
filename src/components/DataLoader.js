import React from "react"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Loader, Message } from "semantic-ui-react"
import { selectIsAuth } from "../redux/auth/auth.selectors"
import { fetchDataAsync } from "../redux/data/data.actions"
import {
	selectIsDataLoaded,
	selectIsDataFetching,
	selectDataError,
} from "../redux/data/data.selector"

const DataLoader = ({ children }) => {
	const isAuth = useSelector(selectIsAuth)
	const isDataLoaded = useSelector(selectIsDataLoaded)
	const isDataFetching = useSelector(selectIsDataFetching)
	const dataError = useSelector(selectDataError)

	const dispatch = useDispatch()

	useEffect(() => {
		// fetch data to redux
		if (isAuth) {
			dispatch(fetchDataAsync())
		}
	}, [isAuth])

	if (!isAuth) {
		return children
	}

	if (!isDataLoaded || isDataFetching) {
		return <Loader active />
	}

	if (dataError) {
		return <Message error content={dataError} />
	}

	return children
}

export default DataLoader
