import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { matchPath, useLocation } from "react-router-dom"
import { selectIsAuth } from "../redux/auth/auth.selectors"
import { addToLastShowed } from "../redux/data/data.actions"
import ROUTES from "../routes/routes.types"

const HistoryListener = () => {
	const { pathname } = useLocation()
	const isAuth = useSelector(selectIsAuth)
	const dispatch = useDispatch()

	// locations listener
	useEffect(() => {
		if (isAuth) {
			const match = matchPath(pathname, {
				path: ROUTES.PLAYER,
				exact: true,
			})

			if (match && match.isExact) {
				// dispatch the action to add the current contentId to the list of the last showed
				dispatch(addToLastShowed(match.params.id))
			}
		}
	}, [pathname, isAuth])

	return null
}

export default HistoryListener
