import React from "react"
import { useSelector } from "react-redux"
import { Grid, Header, Segment } from "semantic-ui-react"
import { selectUserLastShowed } from "../../redux/data/data.selector"
import MovieCard from "../cards/MovieCard"

const UserLastShowedContent = () => {
	const usersLastShowed = useSelector(selectUserLastShowed)

	return (
		<Segment>
			<Header as="h2" content="Últimos vistos" />
			<Grid doubling columns="5">
				{usersLastShowed.map((contentProps) => (
					<Grid.Column key={contentProps.id}>
						<MovieCard {...contentProps} />
					</Grid.Column>
				))}
			</Grid>
		</Segment>
	)
}

export default UserLastShowedContent
