import React, { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { Divider, Grid, Header, Input, Segment } from "semantic-ui-react"
import { selectContents } from "../../redux/data/data.selector"
import { getFilteredContent } from "../../utils/helpers"
import MovieCard from "../cards/MovieCard"

const ContentComponent = () => {
	const content = useSelector(selectContents)
	const [searchText, setSearchText] = useState("")
	const [contentToShow, setContentToShow] = useState(content)

	const onSearchChange = (e, { value }) => {
		setSearchText(value)
	}

	useEffect(() => {
		if (searchText.length === 0) {
			setContentToShow(content)
			return
		}

		const filteredContent = getFilteredContent(content, searchText)

		setContentToShow(filteredContent)
	}, [searchText])

	return (
		<Segment>
			<Header as="h2" content="Todo el contenido" />
			<Input
				name="search"
				fluid
				label={`Buscar aquí 👉`}
				onChange={onSearchChange}
			/>
			<Divider />
			<Grid doubling columns="5">
				{contentToShow.map((contentProps) => (
					<Grid.Column key={contentProps.id}>
						<MovieCard {...contentProps} />
					</Grid.Column>
				))}
				{contentToShow.length === 0 && (
					<Grid.Column>
						<MovieCard title="No hay resultados" />
					</Grid.Column>
				)}
			</Grid>
		</Segment>
	)
}

export default ContentComponent
