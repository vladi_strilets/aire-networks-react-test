import React from "react"
import { useSelector } from "react-redux"
import { Grid, Header, Segment } from "semantic-ui-react"
import { selectUsersFavs } from "../../redux/data/data.selector"
import MovieCard from "../cards/MovieCard"

const UserFavContent = () => {
	const usersFavsContent = useSelector(selectUsersFavs)

	return (
		<Segment>
			<Header as="h2" content="Mis favoritos" />
			<Grid doubling columns="5">
				{usersFavsContent.map((contentProps) => (
					<Grid.Column key={contentProps.id}>
						<MovieCard {...contentProps} />
					</Grid.Column>
				))}
				{usersFavsContent.length === 0 && (
					<Grid.Column>
						<MovieCard title="No hay resultados" />
					</Grid.Column>
				)}
			</Grid>
		</Segment>
	)
}

export default UserFavContent
