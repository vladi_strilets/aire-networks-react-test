import Axios from "axios"
import React, { useEffect } from "react"
import { useForm } from "react-hook-form"
import { useDispatch } from "react-redux"
import { Form, Message } from "semantic-ui-react"
import { setUser } from "../../redux/auth/auth.actions"
import { API_URL } from "../../utils/const"
import { hashPassword, validateEmail } from "../../utils/helpers"

const LoginForm = () => {
	const dispatch = useDispatch()

	const {
		register,
		unregister,
		handleSubmit,
		setValue,
		errors,
		setError,
		clearErrors,
		formState: { isSubmitting },
	} = useForm()

	useEffect(() => {
		register(
			{ name: "email" },
			{
				required: "Introduce tu correo electrónico",
				validate: (value) =>
					validateEmail(value)
						? true
						: "Introduce un correo electrónico válido",
			}
		)
		register(
			{ name: "password" },
			{
				required: "Introduce tu contraseña",
			}
		)
		return () => {
			unregister(["email", "password"])
		}
	}, [register, unregister])

	const onChange = (e, { name, value }) => {
		e.preventDefault()
		setValue(name, value)
	}

	const onFocus = (e) => {
		e.preventDefault()
		const name = e.target.name
		if (errors[name]) {
			clearErrors(name)
		}
		if (errors.form) {
			clearErrors("form", { exact: false })
		}
	}

	const onSubmit = async ({ email, password }, e) => {
		e.preventDefault()

		const formData = new FormData()

		formData.append("user", email)
		formData.append("pass", hashPassword(password))
		formData.append("device", "Web")

		try {
			const { data } = await Axios({
				url: `${API_URL}/Login.php`,
				method: "POST",
				data: formData,
			})

			if (data.error) {
				setError("form", {
					type: "manual",
					message: data.message || "Se ha producido un error",
				})
				return
			}

			if (!data.authorized) {
				setError("form", {
					type: "manual",
					message: "No estás autorizado",
				})
				return
			}

			const user = {
				email,
				token: data.token,
			}

			dispatch(setUser(user))
		} catch (err) {
			console.error(err.message)
			setError("form", {
				type: "manual",
				message: err.message,
			})
		}
	}

	return (
		<Form onSubmit={handleSubmit(onSubmit)} noValidate>
			<Form.Input
				label="Email"
				name="email"
				fluid
				type="email"
				placeholder="juan@gmail.com"
				onChange={onChange}
				onFocus={onFocus}
				error={errors.email ? errors.email.message : false}
			/>
			<Form.Input
				label="Contraseña"
				name="password"
				fluid
				type="password"
				placeholder="Tu contraseña"
				onChange={onChange}
				onFocus={onFocus}
				error={errors.password ? errors.password.message : false}
			/>
			{errors.form && <Message negative>{errors.form.message}</Message>}
			<Form.Button
				type="submit"
				primary
				fluid
				{...(isSubmitting && { loading: true, disabled: true })}
			>
				Entrar
			</Form.Button>
		</Form>
	)
}

export default LoginForm
