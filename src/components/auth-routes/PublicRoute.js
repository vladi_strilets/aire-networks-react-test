import React from "react"
import { useSelector } from "react-redux"
import { Redirect, Route } from "react-router-dom"
import { selectIsAuth } from "../../redux/auth/auth.selectors"
import ROUTES from "../../routes/routes.types"

const RedirectToHome = () => <Redirect to={ROUTES.HOME} />

const PublicRoute = ({ restricted = false, ...props }) => {
	// restricted means is the logged user can visit the page
	// restricted = true  ----> logged user can't visit it

	const isAuth = useSelector(selectIsAuth)

	if (isAuth && restricted) {
		return <Route {...props} component={RedirectToHome} />
	}

	return <Route {...props} />
}

export default PublicRoute
