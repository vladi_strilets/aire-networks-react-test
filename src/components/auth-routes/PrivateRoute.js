import React from "react"
import { useSelector } from "react-redux"
import { Redirect, Route } from "react-router-dom"
import { selectIsAuth } from "../../redux/auth/auth.selectors"
import ROUTES from "../../routes/routes.types"

const RedirectToLogin = () => <Redirect to={ROUTES.LOGIN} />

const PrivateRoute = (props) => {
	const isAuth = useSelector(selectIsAuth)

	if (isAuth) {
		return <Route {...props} />
	}

	return <Route {...props} component={RedirectToLogin} />
}

export default PrivateRoute
