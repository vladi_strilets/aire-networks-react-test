import React from "react"
import ReactHlsPlayer from "react-hls-player"
import { useSelector } from "react-redux"
import { selectPlayerUrl } from "../../redux/player/player.selectors"

const Player = () => {
	const url = useSelector(selectPlayerUrl)

	return (
		<ReactHlsPlayer
			url={url}
			autoplay={false}
			controls={true}
			width="100%"
			height="auto"
		/>
	)
}

export default Player
