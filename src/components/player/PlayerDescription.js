import moment from "moment"
import React, { useMemo } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Button, Grid, Header, Image, Segment } from "semantic-ui-react"
import { addToFavs, removeFromFavs } from "../../redux/data/data.actions"
import { makeIsFavSelector } from "../../redux/data/data.selector"
import { selectPlayerData } from "../../redux/player/player.selectors"

const PlayerDescription = () => {
	const playerData = useSelector(selectPlayerData)
	const {
		id,
		cover,
		title,
		section,
		rating,
		votes,
		totalVotes,
		duration,
	} = playerData

	const dispatch = useDispatch()

	const selectIsFav = useMemo(makeIsFavSelector, [])
	const isFav = useSelector((state) => selectIsFav(state, id))

	const formattedDuration = moment.utc(duration * 1000).format("HH:mm:ss")

	const onButtonClick = () => {
		if (isFav) {
			dispatch(removeFromFavs(id))
		} else {
			dispatch(addToFavs(id))
		}
	}

	return (
		<Segment vertical>
			<Grid>
				<Grid.Column width="4">
					<Image src={cover} fluid />
				</Grid.Column>
				<Grid.Column width="12">
					<Header as="h1" content={title} />
					<Header as="h4" content={`Categoría: ${section}`} />
					<Header as="h4" content={`Rating: ${rating}`} />
					<Header as="h4" content={`Duración: ${formattedDuration}`} />
					<Header as="h4" content={`Votos: ${votes}/${totalVotes}`} />
					<Button
						basic
						content={isFav ? "Eliminar de favoritos" : "Añadir a favoritos"}
						onClick={onButtonClick}
						color={isFav ? "red" : "blue"}
					/>
				</Grid.Column>
			</Grid>
		</Segment>
	)
}

export default PlayerDescription
