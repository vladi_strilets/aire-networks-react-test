import React, { useCallback, useState } from "react"
import { useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Search } from "semantic-ui-react"
import { selectContents } from "../../redux/data/data.selector"
import ROUTES from "../../routes/routes.types"
import { getFilteredContent } from "../../utils/helpers"

const INITIAL_STATE = {
	results: [],
	value: "",
}

const HeaderSearch = () => {
	const contents = useSelector(selectContents)
	const contentsForSearch = contents.map((content) => ({
		id: content.id,
		title: content.title,
		image: content.cover,
		description: content.section,
	}))

	const history = useHistory()

	const [state, setState] = useState(INITIAL_STATE)
	const { results, value } = state

	const handleSearchChange = useCallback((e, data) => {
		setState((state) => ({ ...state, value: data.value }))

		if (data.value.length === 0) {
			setState(INITIAL_STATE)
			return
		}

		setState((state) => ({
			...state,
			results: getFilteredContent(contentsForSearch, data.value).slice(0, 10),
		}))
	}, [])

	const onResultSelect = (e, data) => {
		history.push(ROUTES.PLAYER.replace(":id", data.result.id))
		setState(INITIAL_STATE)
	}

	return (
		<Search
			onResultSelect={onResultSelect}
			onSearchChange={handleSearchChange}
			results={results}
			value={value}
			noResultsMessage="No hay resultados"
			placeholder="Buscar aquí"
		/>
	)
}

export default HeaderSearch
