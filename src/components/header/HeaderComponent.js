import React from "react"
import { batch, useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { Button, Image, Menu } from "semantic-ui-react"
import { logout } from "../../redux/auth/auth.actions"
import {
	selectIsAuth,
	selectUserAvatar,
	selectUserName,
} from "../../redux/auth/auth.selectors"
import { removeData } from "../../redux/data/data.actions"
import ROUTES from "../../routes/routes.types"
import HeaderSearch from "./HeaderSearch"

const MenuRight = () => {
	const userAvatar = useSelector(selectUserAvatar)
	const userName = useSelector(selectUserName)

	const dispatch = useDispatch()

	const signOut = () => {
		batch(() => {
			dispatch(logout())
			dispatch(removeData())
		})
	}

	return (
		<Menu.Menu position="right">
			<Menu.Item>
				<HeaderSearch />
			</Menu.Item>
			<Menu.Item>
				<span>{userName}</span>
				<Image src={userAvatar} avatar bordered inline floated="right" />
			</Menu.Item>
			<Menu.Item>
				<Button basic content="Salir" onClick={signOut} />
			</Menu.Item>
		</Menu.Menu>
	)
}

const HeaderComponent = () => {
	const isAuth = useSelector(selectIsAuth)

	if (!isAuth) {
		return null
	}

	return (
		<Menu borderless>
			<Menu.Item name="Home" as={Link} to={ROUTES.HOME} />
			<MenuRight />
		</Menu>
	)
}

export default HeaderComponent
