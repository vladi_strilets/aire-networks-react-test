import React from "react"
import { Provider } from "react-redux"
import { persistor, store } from "./redux/store"
import { PersistGate } from "redux-persist/integration/react"
import { BrowserRouter } from "react-router-dom"
import MainRoutes from "./routes/main.routes"
import DataLoader from "./components/DataLoader"
import { Loader } from "semantic-ui-react"
import HeaderComponent from "./components/header/HeaderComponent"
import HistoryListener from "./components/HistoryListener"

const App = () => {
	return (
		<Provider store={store}>
			<PersistGate loading={<Loader active />} persistor={persistor}>
				<BrowserRouter>
					<DataLoader>
						<HistoryListener />
						<HeaderComponent />
						<MainRoutes />
					</DataLoader>
				</BrowserRouter>
			</PersistGate>
		</Provider>
	)
}

export default App
