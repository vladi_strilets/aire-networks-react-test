module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	extends: ["eslint:recommended", "plugin:react/recommended"],
	parser: "babel-eslint",
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: 2018,
		sourceType: "module",
	},
	plugins: ["react"],
	rules: {
		strict: 0,
		indent: [1, "tab", { SwitchCase: 1 }],
		semi: ["error", "never"],
		"no-unused-vars": "warn",
		"no-empty-pattern": "warn",
		"react/prop-types": "warn",
	},
}
